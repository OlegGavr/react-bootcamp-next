import React from 'react';
import Link from 'next/link'
import styles from '../styles/Home.module.css'

function PublicRoom({data}) {
  console.log("data", data);
  return (
    <div className={styles.main}>
      <div className={styles.description}>
        Вы в публичной комнате
      </div>
      <div>
        {
          data.map(item => <div key={item.id}>{item.name}</div>)
        }
      </div>


        <Link passHref href="/">
          <div className={styles.card}>
            <h2 className={styles.description}>Назад</h2>
          </div>
        </Link>
    </div>
  )
};

export async function getServerSideProps() {
  const res = await fetch("https://reqres.in/api/products")
  const data = await res.json()

  return { props: { data: data.data } }
}

export default PublicRoom;