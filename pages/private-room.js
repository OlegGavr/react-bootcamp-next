import React from 'react';
import Link from 'next/link'
import withPrivateRoute from '../components/privateRoute';
import styles from '../styles/Home.module.css'

const PrivateRoom = ({userAuth}) => {
  console.log("userAuth", userAuth);

  return userAuth && (
    <div className={styles.main}>
      <div className={styles.description}>
        Вы в приватной комнате
      </div>

      <Link passHref href="/">
          <div className={styles.card}>
            <h2 className={styles.description}>Назад</h2>
          </div>
        </Link>
    </div>
  )
};

export default withPrivateRoute(PrivateRoom);
